/*
Created by Daniel Nadeau
daniel.nadeau01@gmail.com
danielnadeau.blogspot.com

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
package com.echo.elementaryoswallpaper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import com.echo.elementaryoswallpaper.util.SkuDetails;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Utilities {

	static String file_purchase = "purchase.txt";
	static String dir = Environment.getExternalStorageDirectory().getPath()+"/elementary/wallpaper/";
	
	public static AlertDialog getDonateDialog(final Activity_PaperPicker c, final ArrayList<SkuDetails> skus) {
		AlertDialog.Builder b = new AlertDialog.Builder(c);
		View v = c.getLayoutInflater().inflate(R.layout.dialog_donate, null);
		final RadioGroup group = (RadioGroup) v.findViewById(R.id.radioGroup);
		group.setOrientation(RadioGroup.VERTICAL);
		int count = 0;
		for (SkuDetails sku : skus) {
			RadioButton r = new RadioButton(c);
			r.setText(sku.getPrice()+" - "+sku.getTitle().split(" \\(")[0]);
			r.setId(count);
			r.setPadding(0, 14, 0, 14);
			group.addView(r);
			count++;
		}
		
		b.setPositiveButton(R.string.menu_donate, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				c.startPurchase(skus.get(group.getCheckedRadioButtonId()));
			}
		});
		b.setNegativeButton(R.string.cancel, null);
		b.setTitle(R.string.menu_donate);
		b.setView(v);
		return b.create();
		
	}
	
	static void writePurchased(Context c) {
		try {
			FileOutputStream fos = c.openFileOutput(file_purchase, Context.MODE_PRIVATE);
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static boolean isPurchased(Context c) {
		try {
			FileInputStream fis = c.openFileInput(file_purchase);
			fis.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	static ArrayList<Wallpaper> getWallpaperList(){
		ArrayList<Wallpaper> papers = new ArrayList<Wallpaper>();
    	
    	int count = 1;
        while (true){
        	try {
                Class res = R.drawable.class;
                Field field = res.getField("wall_"+count);
                int drawableId = field.getInt(null);
                Wallpaper paper = new Wallpaper();
                paper.setName("wall_"+count);
                paper.setWallpaperId(drawableId);
                papers.add(paper);
                count++;
            } catch (Exception e) {
                break;
            }
        }
        
        return papers;
	}
	
	public static int getWallpaperId(int wallpaperNumber) {
        	try {
                Class res = R.drawable.class;
                Field field = res.getField("wall_"+wallpaperNumber);
                int drawableId = field.getInt(null);
                return drawableId;
            } catch (Exception e) {
                return 0;
            }
	}
    
    public static void moveFilesToSD(Resources resources){
    	int count = 1;
    	Bitmap b = null;
    	File f = new File(dir);
    	f.mkdirs();
    	
        while (true){
        	try {
                f = new File(dir+"wall_"+count+".jpeg");
                if (!f.exists()){
                	Class res = R.drawable.class;
                    Field field = res.getField("wall_"+count);
                    int drawableId = field.getInt(null);
                    b = BitmapFactory.decodeResource(resources, drawableId);
                    b.compress(CompressFormat.JPEG, 100, new FileOutputStream(f));
                }
                count++;
            } catch (Exception e) {
                break;
            }
        }
    }
    
    static int dpToPx(Resources res, int dp) {
        DisplayMetrics displayMetrics = res.getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));       
        return px;
    }
    
}
