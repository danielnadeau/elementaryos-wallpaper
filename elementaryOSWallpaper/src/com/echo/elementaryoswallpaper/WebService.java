/*
Created by Daniel Nadeau
daniel.nadeau01@gmail.com
danielnadeau.blogspot.com

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
package com.echo.elementaryoswallpaper;

import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public class WebService{
	public interface IWebService {
	
		@GET("/system")
		String getSystemName(@Query("identifier") String identifier);
		
		@GET("/wallpaper/current")
		WallpaperResponse getCurrentWallpaper(@Query("identifier") String identifier);
		
		@POST("/wallpaper/set")
		int setWallpaper(@Query("identifier") String identifier, @Query("wallpaper") int wallpaperId);
		
	}
	
	static class WallpaperResponse {
		int wallpaper;
		String system_name;
	}
}