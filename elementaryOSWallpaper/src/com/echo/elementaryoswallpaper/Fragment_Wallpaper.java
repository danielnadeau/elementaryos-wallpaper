/*
Created by Daniel Nadeau
daniel.nadeau01@gmail.com
danielnadeau.blogspot.com

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
package com.echo.elementaryoswallpaper;

import java.io.File;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
import com.androidquery.AQuery;

public class Fragment_Wallpaper extends SherlockFragment {

	private static final String IMAGE_DATA_EXTRA = "resId", WIDTH_EXTRA = "width";
    private String mImageName;
    private ImageView mImageView;
    private AQuery aq;
    private int width = 400;
    
    static Fragment_Wallpaper newInstance(String imageNum, int width) {
        final Fragment_Wallpaper f = new Fragment_Wallpaper();
        final Bundle args = new Bundle();
        args.putString(IMAGE_DATA_EXTRA, imageNum);
        args.putInt(WIDTH_EXTRA, width);
        f.setArguments(args);
        return f;
    }

    // Empty constructor, required as per Fragment docs
    public Fragment_Wallpaper() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageName = getArguments() != null ? getArguments().getString(IMAGE_DATA_EXTRA) : "";
        width = getArguments() != null? getArguments().getInt(WIDTH_EXTRA, 400) : 400;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // image_detail_fragment.xml contains just an ImageView
        final View v = inflater.inflate(R.layout.fragment_wallpaper, container, false);
        mImageView = (ImageView) v.findViewById(R.id.imageView1);
        aq = new AQuery(v);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //mImageView.setImageBitmap(Utilities.decodeSampledBitmapFromResource(getResources(), mImageNum, 1200)); // Load image into ImageView
        Log.d("elementary", "name:" + mImageName);
        aq.id(R.id.imageView1).image(new File(Utilities.dir+mImageName+".jpeg"), width-100);

    }

	
	
}
