/*
Created by Daniel Nadeau
daniel.nadeau01@gmail.com
danielnadeau.blogspot.com

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
package com.echo.elementaryoswallpaper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.echo.elementaryoswallpaper.util.IabHelper;
import com.echo.elementaryoswallpaper.util.IabResult;
import com.echo.elementaryoswallpaper.util.Inventory;
import com.echo.elementaryoswallpaper.util.Purchase;
import com.echo.elementaryoswallpaper.util.SkuDetails;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.v4.app.NavUtils;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class Activity_PaperPicker extends SherlockActivity {
	GridView list;
	ProgressDialog d;
	ArrayList<Wallpaper> papers = null;
	int width = 600;
	
	IabHelper mHelper;
    String SKU_00100 = "com.echo.donate.1";
    String SKU_00500 = "com.echo.donate.5";
    String SKU_01000 = "com.echo.donate.10";
    String SKU_02500 = "com.echo.donate.25";
    String SKU_05000 = "com.echo.donate.50";
    String SKU_10000 = "com.echo.donate.100";
    ArrayList<SkuDetails> skus = new ArrayList<SkuDetails>();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paper_picker);
        
        width = Utilities.dpToPx(getResources(), 200);
        
        
        ActionBar ab = this.getSupportActionBar();
        ab.setDisplayUseLogoEnabled(true);
        ab.setLogo(R.drawable.logo);
        ab.setDisplayShowTitleEnabled(false);
        
        list = (GridView)this.findViewById(R.id.cardsList);
        list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent i = new Intent(Activity_PaperPicker.this, Activity_PaperDisplay.class);
				i.putExtra(Activity_PaperDisplay.TAG_INDEX, arg2);
				Activity_PaperPicker.this.startActivity(i);
				
			}
        	
        });
        
        list.setOnItemLongClickListener(new OnItemLongClickListener(){

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int arg2, long arg3) {
				AlertDialog.Builder b = new AlertDialog.Builder(Activity_PaperPicker.this);
				b.setTitle(R.string.set_wallpaper);
				b.setMessage(R.string.confirm_wallpaper);
				b.setPositiveButton(R.string.confirm, new OnClickListener(){

					@Override
					public void onClick(DialogInterface dialog, int which) {
						WallpaperManager myWallpaperManager = WallpaperManager.getInstance(getApplicationContext());
			            try {
			                myWallpaperManager.setResource(papers.get(arg2).getWallpaperId());
			                Message m = new Message();
			                m.arg1 = 2;
			                handler.sendMessage(m);
			                startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
			            } catch (IOException e) {
			            	Message m = new Message();
			                m.arg1 = 3;
			                handler.sendMessage(m);
			            	e.printStackTrace();
			            }
						
					}
					
				});
				b.setNegativeButton(R.string.cancel, null);
				b.create().show();
				return true;
			}
        	
        });
        
        
        
        startLoadFromCache();
        
        mHelper = new IabHelper(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAigllvEAJgH6GQmWxNzSd1bpji13+34/0QNYEjZMa1Qqayxj8taNu73PGHyTfVem7xjOrajKwN69iJx6lAPTAiLAlb3g2cuE9lDKTKAC44fQigr9qqkJ8oqTkb8Lon4YCJuVVzNqeq6wD1uTc9TfVOAkeDFBtkBwFMQ+UgYeLGpcPOVS3opykOoZMNwHLGn0YnfmvQvd33KIThZT3Ah/pZkHEjfN0r0VV9YwvXTR58NPJXHDbUV5K7UBDV5tM0j+LXbnqrpTzvyMf2FQHNN8tZpwYsVN5X7F3z0fIYF6G36qOzw6soTrKfJMESkVQBSWUb0XagboNqXQqXzs2VzWPwQIDAQAB");
        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {
                    return;
                }

                // Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
                List<String> additionalSkuList = new ArrayList<String>();
                additionalSkuList.add(SKU_00100);
                additionalSkuList.add(SKU_00500);
                additionalSkuList.add(SKU_01000);
                additionalSkuList.add(SKU_02500);
                additionalSkuList.add(SKU_05000);
                additionalSkuList.add(SKU_10000);
                mHelper.queryInventoryAsync(true, additionalSkuList, mQueryFinishedListener);
            }
        });
        
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
        	
        }
    }
    
    public void startPurchase(SkuDetails sku) {
		mHelper.launchPurchaseFlow(this, sku.getSku(), 10001, mPurchaseFinishedListener);
	}
    
 // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                System.out.println("Error purchasing: " + result);
                return;
            }
            
            //Toast.makeText(Activity_PaperPicker.this, R.string.i_love_you, Toast.LENGTH_LONG).show();

            
        }
    };
    
	// Listener that's called when we finish querying the items we own
    IabHelper.QueryInventoryFinishedListener mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (result.isFailure()) {
                return;
            }

            if (inventory.hasPurchase(SKU_00100) || inventory.hasPurchase(SKU_00500) || inventory.hasPurchase(SKU_01000)
            		|| inventory.hasPurchase(SKU_02500) || inventory.hasPurchase(SKU_05000) || inventory.hasPurchase(SKU_10000)) {
            	Utilities.writePurchased(Activity_PaperPicker.this);
            }
            
            skus.add(inventory.getSkuDetails(SKU_00100));
            skus.add(inventory.getSkuDetails(SKU_00500));
            skus.add(inventory.getSkuDetails(SKU_01000));
            skus.add(inventory.getSkuDetails(SKU_02500));
            skus.add(inventory.getSkuDetails(SKU_05000));
            skus.add(inventory.getSkuDetails(SKU_10000));
        }
    };
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.menu_paperpicker, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menuitem_about:
	        	AlertDialog.Builder b = new AlertDialog.Builder(this);
	        	View v = this.getLayoutInflater().inflate(R.layout.dialog_about, null);
	        	b.setView(v);
	        	b.setTitle(R.string.about);
	        	b.setPositiveButton(R.string.close, null);
	        	Dialog d = b.create();
	        	d.setOwnerActivity(this);
	        	d.show();
	            return true;
	            
	        case R.id.menuitem_donate:
	        	AlertDialog d3 = Utilities.getDonateDialog(this, skus);
	        	d3.setOwnerActivity(this);
	        	d3.show();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
    
    private void startLoadFromCache() {
		Runnable r = new Runnable(){

			@Override
			public void run() {
				Message m = new Message();
		    	m.arg1 = 0;
		    	handler.sendMessage(m);
				
				papers = Utilities.getWallpaperList();
				Utilities.moveFilesToSD(getResources());
				
				m = new Message();
		    	m.arg1 = 1;
		    	handler.sendMessage(m);
			}
			
		};
		
		Thread t = new Thread(r);
		t.start();
	}

    private Handler handler = new Handler(){
		@Override
		public void handleMessage(Message m){
			if (m.arg1 == 0 && d == null){
				d = new ProgressDialog(Activity_PaperPicker.this);
				d.setMessage(Activity_PaperPicker.this.getResources().getString(R.string.moving_wallpapers));
				d.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				d.setCancelable(false);
				d.setOwnerActivity(Activity_PaperPicker.this);
				d.show();
			} else if (m.arg1 == 1) {
				if (d != null) {
					d.dismiss();
				}
				
				AQuery aq = new AQuery(Activity_PaperPicker.this);
		        aq.id(R.id.cardsList).adapter(new WallpaperAdapter(Activity_PaperPicker.this, 0, papers));
			} else if (m.arg1 == 2) {
				Toast.makeText(Activity_PaperPicker.this, getResources().getString(R.string.wallpaper_success), Toast.LENGTH_SHORT).show();
				startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
			} else if (m.arg1 == 3) {
				Toast.makeText(Activity_PaperPicker.this, getResources().getString(R.string.wallpaper_failure), Toast.LENGTH_SHORT).show();
			}
		}
	};
    
	public class WallpaperAdapter extends ArrayAdapter<Wallpaper> {

	    private final ArrayList<Wallpaper> items;
	    LayoutInflater inflater;
	    Activity a;
	    AQuery aq;
	    
	    public WallpaperAdapter(final Activity activity, final int textViewResourceId, final ArrayList<Wallpaper> items) {
	        super(activity, textViewResourceId, items);
	        this.items = items;
	        inflater = activity.getLayoutInflater();
	        a = activity;
	    }

	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        View rowView = convertView;
	        PreviewView previewView = null;
	 
	        if(rowView == null) {
	            rowView = inflater.inflate(R.layout.card_wallpaper, null);
	            
	            previewView = new PreviewView();
	            previewView.image = (ImageView) rowView.findViewById(R.id.imageView2);
	            rowView.setTag(previewView);
	        } else {
	        	previewView = (PreviewView) rowView.getTag();
	        }
	        
	        aq = new AQuery (convertView);
	        aq.id(previewView.image).image(new File(Utilities.dir+"wall_"+(position+1)+".jpeg"), true, width, null);
	        return rowView;
	    }
	    
	    protected class PreviewView {
	    	ImageView image;
	    }
	}
    
}
