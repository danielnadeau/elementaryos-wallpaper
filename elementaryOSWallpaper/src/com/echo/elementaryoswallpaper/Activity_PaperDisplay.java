/*
Created by Daniel Nadeau
daniel.nadeau01@gmail.com
danielnadeau.blogspot.com

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
package com.echo.elementaryoswallpaper;

import java.io.IOException;
import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.widget.Toast;

public class Activity_PaperDisplay extends SherlockFragmentActivity {
	
	private ArrayList<Wallpaper> wallpapers;
	private int index = 0;
	static final String TAG_INDEX = "index";
	private int width = 400;
	ViewPager mViewPager;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		getSupportActionBar().setBackgroundDrawable(null);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.logo));
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_transparent)));
		
		Display display = getWindowManager().getDefaultDisplay(); 
		width = display.getWidth();
		if (display.getHeight() > width) {
			width = display.getHeight();
		}
		
		setContentView(R.layout.activity_paper_display);
		index = getIntent().getIntExtra(TAG_INDEX, 0);
		wallpapers = Utilities.getWallpaperList();
		
		PageAdapter adapter = new PageAdapter(this.getSupportFragmentManager());
		for (Wallpaper paper : wallpapers) {
			Fragment_Wallpaper tempFrag = Fragment_Wallpaper.newInstance(paper.getName(), width);
			adapter.addPage(tempFrag);
		}
		
		mViewPager = (ViewPager)this.findViewById(R.id.view_pager);
		mViewPager.setAdapter(adapter);
		mViewPager.setOffscreenPageLimit(4);
		mViewPager.setCurrentItem(index);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.menu_paperdisplay, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menuitem_set_wallpaper:
	        	WallpaperManager myWallpaperManager = WallpaperManager.getInstance(getApplicationContext());
	            try {
	                myWallpaperManager.setResource(wallpapers.get(mViewPager.getCurrentItem()).getWallpaperId());
	                Message m = new Message();
	                m.arg1 = 0;
	                handler.sendMessage(m);
	                
	            } catch (IOException e) {
	            	Message m = new Message();
	                m.arg1 = 1;
	                handler.sendMessage(m);
	                
	                e.printStackTrace();
	            }
	            return true;
	            
	        case android.R.id.home:
	            NavUtils.navigateUpFromSameTask(this);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private Handler handler = new Handler(){
		@Override
		public void handleMessage(Message m){
			if (m.arg1 == 0) {
				Toast.makeText(Activity_PaperDisplay.this, getResources().getString(R.string.wallpaper_success), Toast.LENGTH_SHORT).show();
				startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
			} else if (m.arg1 == 1) {
				Toast.makeText(Activity_PaperDisplay.this, getResources().getString(R.string.wallpaper_failure), Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	public static class PageAdapter extends FragmentPagerAdapter {

		String[] titles = new String[]{};
        private final ArrayList<Fragment> mFrag = new ArrayList<Fragment>();
		
		public PageAdapter(FragmentManager fm) {
			super(fm);
		}

        public void addPage(Fragment frag) {
            mFrag.add(frag);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mFrag.size();
        }

        @Override
        public Fragment getItem(int position) {
            return mFrag.get(position);
        }
        
        @Override
        public String getPageTitle(int position){
        	if (position < titles.length){
        		return titles[position].toUpperCase();
        	} else {
        		return null;
        	}
			
        }
        
        public void setTitles(String[] titles) {
        	this.titles = titles;
        }
    }
}
